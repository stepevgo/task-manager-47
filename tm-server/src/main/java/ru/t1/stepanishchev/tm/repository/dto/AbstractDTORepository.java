package ru.t1.stepanishchev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.repository.dto.IAbstractDTORepository;
import ru.t1.stepanishchev.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public class AbstractDTORepository<M extends AbstractModelDTO> implements IAbstractDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}