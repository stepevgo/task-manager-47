package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.component.ISaltProvider;
import ru.t1.stepanishchev.tm.api.property.IDatabaseProperty;

public interface IPropertyService extends ISaltProvider, IDatabaseProperty {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDBSecondLvlCash();

    @NotNull
    String getDBFactoryClass();

    @NotNull
    String getDBUseQueryCash();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBRegionPrefix();

    @NotNull
    String getDBHazelConfig();

}