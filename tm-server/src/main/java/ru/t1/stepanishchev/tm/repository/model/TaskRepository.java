package ru.t1.stepanishchev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.model.ITaskRepository;
import ru.t1.stepanishchev.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull String userId, @NotNull String sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId ORDER BY :sort";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM Task";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM Task m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}